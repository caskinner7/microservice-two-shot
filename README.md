# Wardrobify

Team:

* Joyce Dionisio - Shoes
* Chris Skinner - Hats

## Design


## Shoes microservice

Utilized the wardrobe api to extract the necessary components in order to formulate my own BinVO model in shoes_rest_.
 I took the closet name, bin number, and bin size field and integrated them into my BinVO model then I used that model
 in order to create a foreign key within my shoe model.

## Hats microservice

Utilize wardrobe api to retrieve proper fields for the location closet.
Create Hat model with fields for style name, fabric, color, image, foreign model key
related to the location data from the wardrobe api.
Use react to render data in front end with functionality to list all model instances,
    create new instances, and delete instances.
