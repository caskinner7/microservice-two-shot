from .views import api_list_shoes, api_show_shoe
from django.urls import path


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_create_shoes"),
    path(
        "bin/<int:bin_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path("shoes/<int:id>/", api_show_shoe, name="api_show_shoes"),
]
