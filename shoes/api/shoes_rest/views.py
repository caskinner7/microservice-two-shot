from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href", "id"]



class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name", "manufacturer", "color", "id", "image"]


    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id == None:
            shoes = Shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_id = content["bin"]
            bin = BinVO.objects.get(id=bin_id)

            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )




@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
    Shoe.objects.filter(id=id).update(**content)

    # copied from get detail
    bin = Shoe.objects.get(id=id)
    return JsonResponse(
        bin,
        encoder=ShoeDetailEncoder,
        safe=False,
    )
