import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
    const [bin, setBin] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [image, setImage] = useState('');
    const [color, setColor] = useState('');





    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }


    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }


    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }


    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.image = image;
        data.bin = bin;


        console.log(data);



        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setImage('');
            setBin('');
        }
    }


    const [bins, setBins] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Shoe</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleModelNameChange} placeholder="model-name" value={modelName} required type="text" id="model_name" name="model_name" className="form-control" />
                                <label htmlFor="model_name">ModelName</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleManufacturerChange} placeholder="manufacturer" value={manufacturer} required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleImageChange} placeholder="Image" value={image} required type="url" id="image" name="image" className="form-control" />
                                <label htmlFor="image">Image Url</label>
                            </div>
                            <div className="mb-3">
                                <select value={bin} onChange={handleBinChange} required id="bin" name="bin" className="form-select">
                                    <option value="">Choose a closet</option>
                                    {bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.id}>
                                                {bin.closet_name}
                                            </option>
                                            // </select>
                                            // <label htmlFor="bins" className="form-label">Bins</label>
                                            // <select onChange={handleBinChange} required id="bin" value={bins} name="bin" className="form-select">
                                            //     <option defaultValue="">Choose a Bin</option>
                                            //     {bins.map(bin => {
                                            //         return (
                                            //             <option key={bin.href} value={bin.id}>
                                            //                 {bin.closet_name}
                                            //             </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ShoeForm;
