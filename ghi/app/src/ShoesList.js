import "./shoes.css";
import React, { useState, useEffect } from "react";

function ShoesList() {

    const [shoeState, setShoeState] = useState([]);

    const loadShoes = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoeState(data.shoes);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadShoes();
    }, []);


    const handleDelete = async (shoeId) => {
        const confirmDelete = window.confirm("Are you sure you want to delete?");
        if (!confirmDelete) {
            return;
        }
        const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            console.log(`Shoe with the id ${shoeId} was deleted`)
        } else {
            const errorData = await response.json();
            console.error('Error deleting shoe', errorData);
        }
    }

    console.log(shoeState)
    return (
        <>
            <h1 className="list-heading" id="shoe-heading">Shoes List</h1>
            <div className="card-container" id="card-cont">
                {shoeState.map(shoe => {
                    const cardWidth = {
                        width: "18rem"
                    }
                    return (
                        <div className="card" style={cardWidth} key={shoe.id} id="card">
                            <img className="card-img-top" src={shoe.image} />
                            <div className="card-body" id="card-bod">
                                <h3 className="card-title">{shoe.model_name}</h3>
                                <p className="card-text">Manufacturer: {shoe.manufacturer}</p>
                                <p className="card-text">Color: {shoe.color}</p>
                            </div>
                            <div className="card-footer" id="footer">
                                Closet: {shoe.bin}
                            </div>
                            <button onClick={() => handleDelete(shoe.id)} className="btn btn-info btn-small">Delete Shoe</button>
                        </div>
                    )
                })}
            </div>
            <div>
                <h1 className="table-heading">For you image hating folks:</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model Name</th>
                            <th>Manufacturer</th>
                            <th>Color</th>
                            <th>Closet Name</th>
                            <th>Delete?</th>

                        </tr>
                    </thead>
                    <tbody>
                        {shoeState.map(shoe => {
                            return (
                                <tr key={shoe.id}>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.color}</td>
                                    <td>{shoe.bin}</td>
                                    <td>
                                        <button onClick={() => handleDelete(shoe.id)} className="btn btn-info btn-small">Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>

    );
}

export default ShoesList;
