import React, {useEffect, useState} from 'react';

function HatForm(){

    const handleSubmit = async(event)=>{
        event.preventDefault();
        const data={};
        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.image = image;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok){
            const newHat = await response.json();

            setStyleName('');
            setFabric('');
            setColor('');
            setImage('');
            setLocation('');
        }
    }

    const [styleName, setStyleName] = useState('');
    const handleStyleNameChange = (event)=> {
        const value = event.target.value;
        setStyleName(value);
    }

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event)=> {
        const value = event.target.value;
        setFabric(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event)=> {
        const value = event.target.value;
        setColor(value);
    }
    const [image, setImage] = useState('');
    const handleImageChange = (event)=> {
        const value = event.target.value;
        setImage(value);
    }
    const [location, setLocation] = useState('');
    const handleLocationChange = (event)=> {
        const value = event.target.value;
        setLocation(value);
    }




    const [locations, setLocations] = useState([]);
    const fetchData = async ()=> {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(()=>{
        fetchData();
    },[]);

    return(
    <>
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create A New Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={styleName} onChange={handleStyleNameChange} placeholder="Style Name" required type="text" id="style_name" name="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" id="color" name="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={image} onChange={handleImageChange} placeholder="Image URL" required type="url" id="image" name="image" className="form-control"/>
                <label htmlFor="image">Image URL</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option selected value="">Choose a closet</option>
                  {locations.map(location=>{
                    return(
                        <option key={location.href} value={location.id}>
                            {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    </>
    );
}


export default HatForm;
