import "./HatsList.css";
import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';


function HatsList() {
    const [hatState, setHatState] = useState([]);

    const loadHats = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')
        if (response.ok) {
            const data = await response.json();
            setHatState(data.hats)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadHats();
    }, []);


    const handleDelete = async (hatId) => {
        const confirmDelete = window.confirm("Are you sure you want to delete?");
        if (!confirmDelete) {
            return;
        }
        const hatUrl = `http://localhost:8090/api/hats/${hatId}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            console.log(`Hat with ID of ${hatId} was deleted`)
        } else {
            const errorData = await response.json();
            console.error('Error deleting hat', errorData);
        }
    }


    return (
        <>
            <h1 className="list-heading">Hats List</h1>
            <div className="card-container ">
                {hatState.map(hat => {
                    const cardWidth = {
                        width: "18rem"
                    }
                    const hatDetail = `/hats/${hat.id}/`
                    return (
                        <div className="card" style={cardWidth} key={hat.id}>
                            <img className="hat-card-img card-img-top" src={hat.image} />
                            <div className="card-body">
                                <h3 className="card-title">{hat.style_name}</h3>
                                <p className="card-text">Fabric: {hat.fabric}</p>
                                <p className="card-text">Color: {hat.color}</p>
                            </div>
                            <div className="btn-container">
                                <Link className="hat-detail-link" to={hatDetail} >
                                    <button className="details-btn btn-primary">
                                        Hat Details
                                    </button>
                                </Link>
                                <button onClick={() => handleDelete(hat.id)} className="delete-btn btn-primary">Delete Hat</button>
                            </div>
                            <div className="card-footer">
                                Location: {hat.location}
                            </div>
                        </div>
                    )
                })}
            </div>
        </>
    )
}

export default HatsList;
